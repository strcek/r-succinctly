# R Succinctly
# sample_7_5.R
# One-factor ANOVA

# CREATE SIMULATION DATA
# Step 1: Each group in separate variable
x1 <- rnorm(30, mean = 40, sd = 8)  # Group 1, mean = 40
x2 <- rnorm(30, mean = 41, sd = 8)  # Group 1, mean = 41
x3 <- rnorm(30, mean = 44, sd = 8)  # Group 1, mean = 44
x4 <- rnorm(30, mean = 45, sd = 8)  # Group 1, mean = 45

# Step 2: Combine vectors into a single data frame
xdf <- data.frame(cbind(x1, x2, x3, x4))  # xdf = "x data frame"

# Step 3: Stack data to get outcome column and group column
xs <- stack(xdf)  # xs = "x stack"

# ONE-FACTOR ANOVA
anova1 <- aov(values ~ ind, data = xs)  # Basic model
summary(anova1)  # Model summary

# POST-HOC COMPARISONS
TukeyHSD(anova1)

# CLEAN UP
rm(list = ls())  # Remove all objects from workspace