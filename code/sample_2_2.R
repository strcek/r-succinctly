# R Succinctly
# sample_2_2.R
# Saving charts in R and RStudio

# CHOOSE GRAPHICS DEVICE

# TO SAVE AS PNG
# EITHER this device for a PNG file (raster graphics)
png(filename = "~/Desktop/bar_a.png",  # Give the full path and name
    width = 900,  # Width of image in pixels
    height = 600)  # Height of image in pixels

# TO SAVE AS PDF
# OR this device for a PDF file (scalable vector graphics)
pdf("bar_b.pdf",  # Save to default directory or errors ensue
    width = 9,  # Width in inches (NOT pixels)
    height = 6)  #Height in inches

# CREATE GRAPHIC
# Then run the command(s) for the graphic
oldpar <- par()  # Stores current graphical parameters
par(oma = c(1, 1, 1, 1))  # Sets outside margins: bottom, left, top, right
par(mar = c(4, 5, 2, 1))  # Sets plot margins
barplot(feeds[order(feeds)],  # Order the bars by descending values
        horiz  = TRUE,  # Make the bars horizontal
        las    = 1,  # las gives orientation of axis labels
        col    = c("beige", "blanchedalmond", "bisque1", "bisque2",
                   "bisque3", "bisque4"),  # Vector of colors for bars
        border = NA,  # No borders on bars
        # Add main title and label for x axis
        main   = "Frequencies of Different Feeds\nin chickwts Dataset",
        xlab   = "Number of Chicks")  

# CLEAN UP
dev.off()  # Turns off graphics device
par(oldpar)  # Restores previous graphics parameters (ignore errors)
rm(list = ls())  # Removes all objects from workspace